<?php
    //fungsi tanpa return value
    function cetak_ganjil(){
        for($i = 0; $i < 100; $i++){
            if($i%2==1){
                echo "$i, ";
            }
        }
    }
    function cetak_genap(){
        for($i = 0; $i <= 100; $i++){
            if($i%2==0){
                echo "$i, ";
            }
        }
    }
    //pemanggilan fungsi
    echo "Bilangan ganjil : "; 
    cetak_ganjil();
    echo "<br><br>";
    echo "Bilangan genap : "; 
    cetak_genap();
?>