<?php
    $arrNilai=array("Naufal"=>100, "Putri"=>90, "Made"=>75, "Steven"=>80, "Caca"=>95, "Inosuke"=>55);
    echo "<b>Array sebelum diurutkan</b>";
    echo"<pre>";
    print_r($arrNilai);
    echo "</pre>";

    sort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan sort ()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    rsort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan rsort ()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    asort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan asort ()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    arsort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan arsort ()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    ksort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan ksort ()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    krsort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan krsort ()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
?>