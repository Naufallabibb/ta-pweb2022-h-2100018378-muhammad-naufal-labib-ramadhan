<html>
    <head>
        <title>
            Form Validasi dengan Javascript
            <link rel="stylesheet" type="text/css" href="formJS.css">
        </title>
        <style>
      *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;  
      }
      html,body{
        display: grid;
        height: 100%;
        width: 100%;
        place-items: center;
        background: linear-gradient(315deg, #77ACF1, #FFF5FD);
      }
      </style>
    </head>
    <body>
        <center>
            <h2>
                Portal UAD
            </h2>
        </center>
        <div class="login">
            <form action="#" method="post" onSubmit="validasi()">
                <div>
                    <label>
                    Nama Lengkap : 
                    </label>
                    <input type="text" name="nama" id="nama"/>
                </div>
                <br>
                <div>
                    <label>
                        Email &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: 
                    </label>
                    
                    <input type="email" name="email" id="email"/>
                </div>
                <br>
                <div>
                    <label>
                        Alamat &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: 
                    </label>
                    
                    <textarea name="alamat" id="alamat" cols="40" rows="5">
                    </textarea>
                </div>
                <br>
                <div>
                    <center>
                    <input type="submit" value="Daftar" class="tombol">
                    <input type="reset" value="Batal" class="tombol">
                    </center>
                </div>
            </form>
        </div>
    </body>
    <script type="text/javascript">
        function validasi(){
            var nama = document.getElementById("nama").value;
            var email = document.getElementById("email").value;
            var alamat = document.getElementById("alamat").value;
            if (nama != "" && email !="" && alamat !=""){
                return true;
            }
            else{
                alert('Anda harus mengisi data dengan lengkap !');
            }

        }

    </script>
</html>